import unittest
from points import Point


class TestPoint(unittest.TestCase):

    def setUp(self):
        self.pt1 = Point(2, 2)
        self.pt2 = Point(5, 6)
        self.pt3 = Point(7, 8)

    def test_init(self):
        self.assertIsInstance(self.pt1, Point)
        self.assertIs(self.pt1.x, 2)
        self.assertIs(self.pt1.y, 2)

    def test_str(self):
        self.assertIsInstance(str(self.pt1), str)
        self.assertEqual(str(self.pt1), "(2, 2)")

    def test_repr(self):
        self.assertIsInstance(repr(self.pt1), str)
        self.assertEqual(repr(self.pt1), "Point(2, 2)")

    def test_eq(self):
        self.assertTrue(self.pt1 == Point(2, 2))
        self.assertFalse(self.pt1 == Point(0, 2))
        self.assertFalse(self.pt1 == Point(0, 0))

    def test_ne(self):
        self.assertTrue(self.pt1 != self.pt2)
        self.assertFalse(self.pt1 != Point(2, 2))

    def test_add(self):
        self.assertEqual(self.pt1 + self.pt2, self.pt3)
        self.assertEqual(self.pt1 + Point(-5, -2), Point(-3, 0))

    def test_sub(self):
        self.assertEqual(self.pt1 - self.pt2, Point(-3, -4))
        self.assertEqual(self.pt1 - self.pt3, Point(-5, -6))

    def test_mul(self):
        self.assertEqual(self.pt1 * self.pt2, 22)
        self.assertEqual(self.pt1 * self.pt3, 30)
        self.assertEqual(self.pt1 * Point(-5, -2), -14)

    def test_cross(self):
        self.assertEqual(self.pt1.cross(self.pt1), 0)
        self.assertEqual(self.pt1.cross(Point(0, 0)), 0)
        self.assertEqual(self.pt1.cross(self.pt2), 2)

    def test_length(self):
        self.assertEqual(Point(-5, 0).length(), 5)
        self.assertEqual(Point(0, 0).length(), 0)
        self.assertEqual(Point(3, 4).length(), 5)

    def tearDown(self): pass


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy