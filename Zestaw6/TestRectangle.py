# coding=utf-8
from rectangles import Rectangle
from points import Point

import unittest


class TestRectangle(unittest.TestCase):
    def setUp(self):
        self.rect1 = Rectangle(2, 2, 5, 5)
        self.rect2 = Rectangle(-1, -2, 5, 6)

    def test_init(self):
        self.assertIsInstance(self.rect1, Rectangle)

    def test_str(self):
        self.assertIsInstance(str(self.rect1), str)
        self.assertEqual(str(self.rect1), "[(2, 2), (5, 5)]")

    def test_repr(self):
        self.assertIsInstance(repr(self.rect1), str)
        self.assertEqual(repr(self.rect1), "Rectangle(2, 2, 5, 5)")

    def test_eq(self):
        self.assertTrue(self.rect1 == Rectangle(2, 2, 5, 5))
        self.assertFalse(self.rect1 == self.rect2)

    def test_ne(self):
        self.assertTrue(self.rect1 != self.rect2)
        self.assertFalse(self.rect1 != Rectangle(2, 2, 5, 5))

    def test_center(self):
        self.assertEqual(self.rect1.center(), Point(3.5, 3.5))
        self.assertEqual(self.rect2.center(), Point(2, 2))

    def test_area(self):
        self.assertEqual(self.rect1.area(), 9)
        self.assertEqual(self.rect2.area(), 48)

    def test_move(self):
        self.assertEqual(self.rect1.move(-1, -1), Rectangle(1, 1, 4, 4))
        self.assertTrue(self.rect2.move(-5, 2) == Rectangle(-6, 0, 0, 8))


if __name__ == '__main__':
    unittest.main()  # uruchamia wszystkie testy
