# coding=utf-8
'''
ZAD. 3.1
    Czy podany kod jest poprawny składniowo w Pythonie?
'''
# poprawny
x = 2 ; y = 3 ;
if (x > y):
    result = x;
else:
    result = y;

#for i in "qwerty": if ord(i) < 100: print i  #bledne zagniezdzenie petli
for i in "qwerty":
    if ord(i) < 100:
        print i


for i in "axby": print ord(i) if ord(i) < 100 else i    #poprawny
