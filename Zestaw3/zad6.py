print('Zad 3.6\n');
x = int(raw_input("Podaj wartosc x: "))
y = int(raw_input("Podaj wartosc y: "))


def generate_line(length, pattern, last_char):
    line = ''
    line += pattern * length
    line += last_char + '\n'
    return line


output = ''
for i in range(y):
    output += generate_line(x, '+---', '+')
    output += generate_line(x, '|   ', '|')

output += generate_line(x, '+---', '+')
print output
