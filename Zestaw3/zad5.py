
try:
    length = int(raw_input("Podaj dlugosc miarki: "))
    pattern = "....|"
    linear = ['|', '0']

    linear[0] += (pattern * length)
    for i in range(length):
        linear[1] += "%5s" % (i + 1)

    output = linear[0] + '\n' + linear[1]
    print output

except ValueError:
    print "Nie poprawana wartosc. Program zakonczyl sie niepowodzeniem"
