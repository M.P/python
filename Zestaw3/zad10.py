print('Zad 3.10\n');

dictionary = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
dictionary1 = dict([('I', 1), ('V', 5)])
dictionary2 = dict(zip([('I', 1), ('V', 5)], [('X', 10), ('L', 50)]))
dictionary3 = dict(I=1, V=5, L=50)
dictionary3['C'] = 100


def roman2int(roman):
    arabic_number = 0
    prev = ''
    for i in xrange(len(roman) - 1, -1, -1):
        if dictionary[roman[i]] < arabic_number and roman[i] != prev:
            arabic_number -= dictionary[roman[i]]
            prev = roman[i]
        else:
            arabic_number += dictionary[roman[i]]
            prev = roman[i]

    return arabic_number


print(roman2int("XL"))
print(roman2int("IIL"))
print(roman2int("MCCCXXI"))
print(roman2int("MCCC"))
print(roman2int("CMXXXV"))
