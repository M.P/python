# -*- coding: utf-8 -*-
def main():
    zad10()
    zad11()
    zad12()
    zad13()
    zad14()
    zad15()
    zad16()
    zad17()
    zad18()
    zad19()


def zad10():
    line = "Mamy \n dany \t napis \nwielowierszowy line."
    words = line.split()
    print "Zad2.10 Liczba wyrazów: ", len(words)


def zad11():
    word = "word"
    print "Zad2.11: ", "_".join(c for c in word)


def zad12():
    line = "Mamy \n dany \t napis \nwielowierszowy line."
    words = line.split()

    list_with_first_chars = []
    list_with_last_chars = []

    for char in words:
        list_with_first_chars.append(char[0])
        list_with_last_chars.append(char[-1])

    print "Zad2.12 Napis z pierwszych znaków wyrazów: ", "".join(list_with_first_chars)
    print "Zad2.12 Napis z ostatnich znaków wyrazów: ", "".join(list_with_last_chars)


def zad13():
    line = "Mamy \n dany \t napis \nwielowierszowy line."
    words = line.split()

    words_length = sum(len(s) for s in words)

    print "Zad2.13 Laczna dlugosc wyrazow: ", words_length


def zad14():
    line = "Mamy \n dany \t napis \nwielowierszowy line."
    words = line.split()

    longest_word = max(words, key=len)
    word_length = len(longest_word)
    print "Zad2.14 Najdłuższy wyraz: {}, długość najdluższego wyrazu: {}".format(longest_word, word_length)


def zad15():
    numbers = [1, 20, 30, 40, 45]
    print "Zad2.15", "".join(str(c) for c in numbers)


def zad16():
    line = "W tekście znajdującym się w zmiennej line zamienić ciąg znaków GvR"
    line = line.replace("GvR", "Guido van Rossum")
    print "Zad2.16", line


def zad17():
    line = "A computer file is a computer resource for recording data discretely in a computer storage device."
    words = line.split()

    sorted_list = sorted(words, key=str.lower)
    print "Zad2.17 Alfabetyczne sortowanie: ", sorted_list
    sorted_list = sorted(words, key=len)
    print "Zad2.18 Sortowanie wzgledem dlugosci: ", sorted_list


def zad18():
    number = 5150000000004810
    print "Zad2.18 Liczba zer:", str(number).count('0'), " w liczbie", number


def zad19():
    numbers = [1, 4, 44, 505, 12, 8, 111, 121, 41]
    numbers_as_string = [str(c).zfill(3) for c in numbers]
    print "Zad2.19", ", ".join(c for c in numbers_as_string)


main()
