def factorial(n):
    product = 1
    for i in xrange(1, n + 1):
        product *= i
    return product


def fibonacci(n):
    if n <= 1:
        return

    fib = 1
    prev_fib = 1

    for i in xrange(2, n):
        temp = fib
        fib += prev_fib
        prev_fib = temp

    return fib