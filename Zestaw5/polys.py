# coding=utf-8
def add_poly(poly1, poly2):  # poly1(x) + poly2(x)
    if len(poly1) < len(poly2):
        temp = poly2
        for i in range(len(poly1)):
            temp[i] += poly1[i]
    else:
        temp = poly1
        for i in range(len(poly2)):
            temp[i] += poly2[i]

    return temp


def sub_poly(poly1, poly2):  # poly1(x) - poly2(x)
    return add_poly(poly1, mul_const(poly2, -1))


def mul_poly(poly1, poly2):  # poly1(x) * poly2(x)
    temp = [0] * (len(poly1) + len(poly2) - 1)
    for i in range(len(poly1)):
        for j in range(len(poly2)):
            temp[i + j] = temp[i + j] + (poly1[i] * poly2[j])
    return temp


def is_zero(poly):  # bool, [0], [0,0], itp.
    for i in poly:
        if i != 0:
            return False
    return True


def cmp_poly(poly1, poly2):  # bool, porównywanie
    if len(poly1) < len(poly2):
        for i in range(len(poly2)):
            if i < len(poly1):
                if poly1[i] != poly2[i]:
                    return False
            else:
                if poly2[i] != 0:
                    return False
        return True
    else:
        for i in range(len(poly1)):
            if i < len(poly2):
                if poly1[i] != poly2[i]:
                    return False
            else:
                if poly1[i] != 0:
                    return False
        return True


def eval_poly(poly, x0):  # poly(x0), algorytm Hornera
    result = 0
    for coefficient in reversed(poly):
        result = result * x0 + coefficient
    return result


def combine_poly(poly1, poly2):  # poly1(poly2(x)), trudne!
    i = len(poly1) - 1
    temp = [poly1[i]]
    while i > 0:
        i = i - 1
        temp = add_poly(mul_poly(temp, poly2), [poly1[i]])
    return temp


def pow_poly(poly, n):  # poly(x) ** n
    temp = [1]
    while n > 0:
        temp = mul_poly(temp, poly)
        n = n - 1
    return temp


def diff_poly(poly):  # pochodna wielomianu
    if len(poly) == 1:
        return [0]

    temp = [poly[i] * i for i in range(1, len(poly))]
    while (len(temp) - 1) > 0 and temp[len(temp) - 1] == 0:
        del temp[len(temp) - 1]

    return temp


def mul_const(poly, const):
    return [const * p for p in poly]
