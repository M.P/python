def binarne_rek(L, left, right, y):
    """Wyszukiwanie binarne w wersji rekurencyjnej."""
    if len(L) == 0:
        raise ValueError("Tablica jest pusta")
    if left > right:
        return None
    k = (left + right) / 2
    if L[k] == y:
        return k
    if y < L[k]:
        return binarne_rek(L, left, k - 1, y)
    else:
        return binarne_rek(L, k + 1, right, y)


if __name__ == '__main__':
    L = [1, 2, 3, 4, 7, 8, 31, 343, 433, 1111]
    L1 = []

    print binarne_rek(L, 0, len(L) - 1, 31)
    print binarne_rek(L1, 0, 1, 2)
