from node import Node


def remove_head(node):
    if node is None:
        raise ValueError("Lista jest pusta")
    return node.next, node


def remove_tail(node):
    if node is None:
        raise ValueError("Lista jest pusta")

    ptr1 = node
    ptr2 = node.next

    if ptr2 is None:    # lista zawiera jeden element
        return None, ptr1

    while ptr2.next:
        ptr1 = ptr2
        ptr2 = ptr2.next

    ptr1.next = None

    return node, ptr2


head = None
head = Node(1, head)
head = Node(2, head)
head = Node(3, head)
# Zastosowanie.
while head is not None:
    #head, node = remove_head(head)
    head, node = remove_tail(head)
    print "usuwam", node