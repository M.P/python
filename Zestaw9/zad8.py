# coding=utf-8
from node_bst import Node


def bst_insert(top, data):  # zwraca nowy korzeń
    if top is None:
        return Node(data)
    if data < top.data:
        top.left = bst_insert(top.left, data)
    elif data > top.data:
        top.right = bst_insert(top.right, data)
    else:
        pass  # ignorujemy duplikaty
    return top  # bez zmian


def bst_max(top):
    if top is None:
        raise ValueError('Drzewo jest puste')
    if top.right is None:
        return top.data
    while top.right:
        top = top.right
    return top.data


def bst_min(top):
    if top is None:
        raise ValueError('Drzewo jest puste')
    if top.left is None:
        return top.data
    while top.left:
        top = top.left
    return top.data


root = Node(4)
root = bst_insert(root, 5)
root = bst_insert(root, 4)
root = bst_insert(root, 3)
root = bst_insert(root, 1)
root = bst_insert(root, 6)
root = bst_insert(root, 7)
root = bst_insert(root, 0)
print bst_min(root)
print bst_max(root)
