# coding=utf-8


def solve1(a, b, c):
    """Rozwiązywanie równania liniowego a x + b y + c = 0."""

    if a == 0 and b == 0:
        if c == 0:
            print "Rownanie posiada nieskonczenie wiele rozwiazan."
        else:
            print "Rownanie jest sprzeczne."
    if a == 0:
        return "y = " + str(-c / b)
    elif b == 0:
        return "x = " + str(-c / a)
    else:
        return 'y = (' + str(-a) + 'x' + str(-c) + ')/' + str(b)