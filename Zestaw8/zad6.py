def recursion_function(i, j):
    if i == 0 and j == 0:
        return 0.5
    if i == 0 and j > 0:
        return 1
    if j == 0 and i > 0:
        return 0
    if j > 0 and i > 0:
        return 0.5 * (recursion_function(i - 1, j) + recursion_function(i, j - 1))


def dynamic_function(a, b):
    p = {(0, 0): 0.5}
    for i in range(a + 1):
        p[(i, 0)] = 0

    for i in range(b + 1):
        p[(0, i)] = 1

    for i in range(1, a + 1):
        for j in range(1, b + 1):
            val = 0.5 * (p[(i - 1, j)] + p[(i, j - 1)])
            p[(i, j)] = val

    return p[(a, b)]
