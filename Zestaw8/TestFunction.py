import unittest
from zad1 import solve1
from zad3 import calc_pi
from zad4 import heron
from zad6 import dynamic_function, recursion_function


class TestFunction(unittest.TestCase):
    def test_calc_pi(self):
        self.assertAlmostEqual(calc_pi(100000), 3.14159265359, places=2)

    def test_solve1(self):
        self.assertEqual(solve1(1, 2, 3), "y = (-1x-3)/2")

    def test_heron(self):
        self.assertEqual(heron(3, 4, 5), 6)
        with self.assertRaises(ValueError):
            heron(1, 2, 5)

    def test_dynamic_and_recursion_function(self):
        self.assertEqual(dynamic_function(5, 8), recursion_function(5, 8))
        self.assertEqual(dynamic_function(2, 3), 0.6875)

    if __name__ == '__main__':
        unittest.main()  # uruchamia wszystkie testy