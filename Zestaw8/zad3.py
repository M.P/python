# coding=utf-8
import random


def calc_pi(n=100):
    """Obliczanie liczby pi metodą Monte Carlo.
    n oznacza liczbę losowanych punktów."""
    circle_points = 0
    square_points = 0
    for i in range(n):
        x = random.uniform(0, 1)
        y = random.uniform(0, 1)
        if (x * x + y * y) <= 1:
            circle_points += 1
        square_points += 1

    return (4.0 * circle_points) / square_points
