import math


def is_triangle(a, b, c):
    tmp = [a, b, c]
    tmp.sort()
    if tmp[0] + tmp[1] >= tmp[-1]:
        return True
    return False


def heron(a, b, c):
    if not is_triangle(a, b, c):
        raise ValueError("Warunek trojkata jest nie spelniony")
    p = (a + b + c) / 2
    return math.sqrt(p * (p - a) * (p - b) * (p - c))
