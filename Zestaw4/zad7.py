print("Zadanie 4.7:")


def flatten(sequence):
    L = []
    for item in sequence:
        if isinstance(item, (list, tuple)):
            L.extend(flatten(item))
        else:
            L.append(item)
    return L


sequence = [1, (2, 3), [], [4, (5, 6, 7)], 8, [9]]
print(flatten(sequence))
