Lista = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


def odwracanie(L, left, right):
    if left < 0 or left >= len(L):
        raise ValueError("Bledna wartosc argumentu left")
    if right < left or right >= len(L):
        raise ValueError("Bledna wartosc argumentu right")

    while left < right:
        L[left], L[right] = L[right], L[left]
        left += 1
        right -= 1


def odwracanie_rekurencyjne(L, left, right):
    if left < right:
        L[left], L[right] = L[right], L[left]
        odwracanie_rekurencyjne(L, left + 1, right - 1)
    else:
        return


print Lista
odwracanie(Lista, 2, 8)
print Lista
odwracanie_rekurencyjne(Lista, 2, 8)
print Lista
