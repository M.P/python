def fibonacci(n):
    if n <= 1:
        return

    fib = 1
    prev_fib = 1

    for i in xrange(2, n):
        temp = fib
        fib += prev_fib
        prev_fib = temp

    return fib


print "Zad4.4 fibonacci(10): ", fibonacci(10)
