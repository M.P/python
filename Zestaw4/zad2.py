def generate_measure(length):
    pattern = "....|"
    linear = ['|', '0']
    linear[0] += (pattern * length)

    for i in range(length):
        linear[1] += "%5s" % (i + 1)
        output = linear[0] + '\n' + linear[1]

    return output


def generate_line(length, pattern, last_char):
    line = ''
    line += pattern * length
    line += last_char + '\n'
    return line


def generate_rectangle(x, y):
    output = ''
    for i in range(y):
        output += generate_line(x, '+---', '+')
        output += generate_line(x, '|   ', '|')

    output += generate_line(x, '+---', '+')
    return output


print 'Zad4.2:'
print generate_measure(12)
print generate_rectangle(5, 5)
