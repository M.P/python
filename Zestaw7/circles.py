# coding=utf-8
from points import Point
import math


class Circle:
    """Klasa reprezentująca okręgi na płaszczyźnie."""

    def __init__(self, x, y, radius):
        if radius < 0:
            raise ValueError("promień ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __repr__(self):  # "Circle(x, y, radius)"
        return "Circle(" + str(self.pt.x) + ", " + str(self.pt.y) + ", " + str(self.radius) + ")"

    def __eq__(self, other):
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):  # pole powierzchni
        return math.pi * self.radius * self.radius

    def move(self, x, y):  # przesuniecie o (x, y)
        self.pt.x += x
        self.pt.y += y
        return self

    def cover(self, other):  # okrąg pokrywający oba
        dist = (math.sqrt((self.pt.x - other.pt.x)**2 + (self.pt.y - other.pt.y)**2))

        if dist <= abs(self.radius - other.radius):
            if self.radius > other.radius:
                return self
            return other
        else:
            new_x = (self.pt.x + other.pt.x) / 2.0
            new_y = (self.pt.y + other.pt.y) / 2.0

            new_radius = dist / 2.0 + max(self.radius, other.radius)

        return Circle(new_x, new_y, new_radius)
