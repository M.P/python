# coding=utf-8
from bfs import BFS
from dfs import DFS
from edge import Edge
from copy import copy, deepcopy
from traverse_graph import *


class Graph:
    """Klasa dla grafu ważonego, skierowanego lub nieskierowanego."""

    def __init__(self, n, directed=False):
        self.n = n  # kompatybilność
        self.directed = directed  # bool, czy graf skierowany
        self.adjacent_matrix = []
        if n > 0:
            for i in range(n):
                self.adjacent_matrix.append([0 for i in range(n)])
        else:
            raise ValueError("Bledna wartosc argumentu konstruktora")
        self.nr_of_edges = 0

    def __eq__(self, other):
        return self.n == other.n and self.directed == other.directed and self.adjacent_matrix == other.adjacent_matrix \
               and self.nr_of_edges == other.nr_of_edges

    def __len__(self):
        return self.n

    def __contains__(self, item):
        return 0 <= item < self.n

    def __iter__(self):
        return iter(range(self.n))

    def __getitem__(self, item):
        # if self.is_directed():
        #     raise ValueError("Graf jest skierowany")
        if item not in self:
            raise ValueError("Wierzcholek nie istnieje.")
        n_vertices = []
        for i in range(self.v()):
            if self.adjacent_matrix[item][i] > 0:
                n_vertices.append(i)
        return n_vertices

    def v(self):  # zwraca liczbę wierzchołków
        return self.n

    def e(self):  # zwraca liczbę krawędzi
        return self.nr_of_edges

    def is_directed(self):  # bool, czy graf skierowany
        return self.directed

    def add_node(self, node):  # dodaje wierzchołek
        if 0 <= node < self.n:
            return True
        else:
            return False

    def has_node(self, node):  # bool
        if 0 <= node < self.n:
            return True
        else:
            return False

    def del_node(self, node):  # usuwa wierzchołek
        if node >= self.v() or node < 0:
            raise ValueError("Nieprawidlowy node")

        for i in range(self.v()):
            if self.is_directed():
                if self.has_edge(Edge(node, i)):
                    self.adjacent_matrix[node][i] = 0
                    self.nr_of_edges -= 1
                if self.has_edge(Edge(i, node)):
                    self.adjacent_matrix[i][node] = 0
                    self.nr_of_edges -= 1
            else:
                if self.has_edge(Edge(node, i)):
                    self.adjacent_matrix[node][i] = 0
                    self.adjacent_matrix[i][node] = 0
                    self.nr_of_edges -= 1

    def add_edge(self, edge):  # wstawienie krawędzi
        source = edge.source
        target = edge.target
        weight = edge.weight
        if  source >= self.v() or source < 0 or target >= self.v() or target < 0:
            raise ValueError("Bledna wartosc source lub target")
        if source == target:
            raise ValueError("Pętle są zabronione")

        if self.is_directed():
            if not self.has_edge(Edge(source, target)):
                self.nr_of_edges += 1
            self.adjacent_matrix[source][target] = weight
        else:
            if not self.has_edge(Edge(source, target)):
                self.nr_of_edges += 1
            self.adjacent_matrix[source][target] = weight
            self.adjacent_matrix[target][source] = weight

    def has_edge(self, edge):  # bool
        source = edge.source
        target = edge.target
        if source < 0 or source >= self.v() or target < 0 or target >= self.v():
            raise ValueError("Nieprawidlowa wartosc source lub target")
        temp_edge = self.adjacent_matrix[source][target]
        if temp_edge > 0:
            return True
        return False

    def del_edge(self, edge):  # usunięcie krawędzi
        if not self.has_edge(edge):
            raise ValueError("Krawedz nie istnieje")
        source = edge.source
        target = edge.target
        if self.is_directed():
            self.adjacent_matrix[source][target] = 0
        else:
            self.adjacent_matrix[source][target] = 0
            self.adjacent_matrix[target][source] = 0
        self.nr_of_edges -= 1

    def weight(self, edge):  # zwraca wagę krawędzi
        source = edge.source
        target = edge.target
        if source < 0 or source >= self.v() or target < 0 or target >= self.v():
            raise ValueError("Nieprawidlowa wartosc source lub target")
        return self.adjacent_matrix[source][target]

    def iternodes(self):  # iterator po wierzchołkach
        return iter(range(self.n))

    def iteradjacent(self, node):  # iterator po wierzchołkach sąsiednich
        adjacent_vertices = []
        for i in range(self.v()):
            if self.has_edge(Edge(node, i)):
                adjacent_vertices.append(i)
        return iter(adjacent_vertices)

    def iteroutedges(self, node):  # iterator po krawędziach wychodzących
        adjacent_edges = []
        for i in range(self.v()):
            if self.has_edge(Edge(node, i)):
                adjacent_edges.append(Edge(node, i, self.adjacent_matrix[node][i]))
        return iter(adjacent_edges)

    def iterinedges(self, node):  # iterator po krawędziach przychodzących
        adjacent_edges = []
        for i in range(self.v()):
            if self.has_edge(Edge(i, node)):
                adjacent_edges.append(Edge(i, node, self.adjacent_matrix[i][node]))
        return iter(adjacent_edges)

    def iteredges(self):  # iterator po krawędziach
        adjacent_edges = []
        for i in range(self.v()):
            for j in range(self.v()):
                if self.has_edge(Edge(i, j)):
                    adjacent_edges.append(Edge(i, j, self.adjacent_matrix[i][j]))
        return iter(adjacent_edges)

    def copy(self):  # zwraca kopię grafu
        graph = Graph(self.n, self.directed)
        graph.adjacent_matrix = deepcopy(self.adjacent_matrix)
        graph.nr_of_edges = self.nr_of_edges
        return graph

    def transpose(self):  # zwraca graf transponowany
        graph = self.copy()

        if graph.is_directed():
            adjacent_matrix = []
            for i in range(graph.v()):
                adjacent_matrix.append([0 for i in range(graph.v())])
            for i in range(graph.v()):
                for j in range(graph.v()):
                    if graph.has_edge(Edge(i, j)):
                        adjacent_matrix[j][i] = graph.adjacent_matrix[i][j]
            graph.adjacent_matrix = adjacent_matrix

        return graph

    def complement(self):  # zwraca dopełnienie grafu
        graph = self.copy()
        edges = []
        nr_of_edges = 0
        for i in range(graph.v()):
            edges.append([0 for i in range(graph.v())])
        graph.adjacent_matrix = edges
        for i in range(graph.v()):
            for j in range(graph.v()):
                if not self.has_edge(Edge(i, j)) and i != j:
                    graph.add_edge(Edge(i, j))
                    nr_of_edges += 1
        graph.nr_of_edges = nr_of_edges
        return graph

    def subgraph(self, nodes):  # zwraca podgraf indukowany
        graph = self.copy()
        for node in nodes:
            graph.del_node(node)
        return graph

    def iter_bfs(self, start_node):
        return BFS(self, start_node)

    def iter_dfs(self, start_node):
        return DFS(self, start_node)

    def traverse_dfs(self, start, visit=visit, visited=None):
        traverse_dfs(self, start, visit, visited=visited)

    def traverse_bfs(self, start, visit=visit):
        traverse_bfs(self, start, visit)
