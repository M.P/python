import unittest
from graph import Graph
from edge import Edge


class TestGraph(unittest.TestCase):

    def setUp(self):
        self.graph = Graph(17, directed=True)
        self.graph_undirected = Graph(17)

        self.graph.add_edge(Edge(0, 1))
        self.graph.add_edge(Edge(0, 2))
        self.graph.add_edge(Edge(1, 3))
        self.graph.add_edge(Edge(1, 4))
        self.graph.add_edge(Edge(2, 5))
        self.graph.add_edge(Edge(2, 6, 5))
        self.graph.add_edge(Edge(2, 7))
        self.graph.add_edge(Edge(3, 8))
        self.graph.add_edge(Edge(4, 9))
        self.graph.add_edge(Edge(4, 10, 10))
        self.graph.add_edge(Edge(5, 11))
        self.graph.add_edge(Edge(5, 12))
        self.graph.add_edge(Edge(7, 13))
        self.graph.add_edge(Edge(8, 14))
        self.graph.add_edge(Edge(11, 15))
        self.graph.add_edge(Edge(12, 16))

        self.graph_undirected.add_edge(Edge(0, 1))
        self.graph_undirected.add_edge(Edge(0, 2))
        self.graph_undirected.add_edge(Edge(1, 3))
        self.graph_undirected.add_edge(Edge(1, 4))
        self.graph_undirected.add_edge(Edge(2, 5))
        self.graph_undirected.add_edge(Edge(2, 6, 5))
        self.graph_undirected.add_edge(Edge(2, 7))
        self.graph_undirected.add_edge(Edge(3, 8))
        self.graph_undirected.add_edge(Edge(4, 9))
        self.graph_undirected.add_edge(Edge(4, 10, 10))
        self.graph_undirected.add_edge(Edge(5, 11))
        self.graph_undirected.add_edge(Edge(5, 12))
        self.graph_undirected.add_edge(Edge(7, 13))
        self.graph_undirected.add_edge(Edge(8, 14))
        self.graph_undirected.add_edge(Edge(11, 15))
        self.graph_undirected.add_edge(Edge(12, 16))

    def test_init(self):
        self.assertNotEqual(self.graph, self.graph_undirected)

    def test_is_directed(self):
        self.assertTrue(self.graph.is_directed())
        self.assertFalse(self.graph_undirected.is_directed())

    def test_add_node(self):
        self.assertTrue(self.graph.add_node(10))
        self.assertTrue(self.graph_undirected.add_node(10))
        self.assertFalse(self.graph.add_node(17))
        self.assertFalse(self.graph_undirected.add_node(122))
        self.assertEqual(17, self.graph.v())

    def test_del_node(self):
        self.assertTrue(self.graph.has_edge(Edge(5, 11)))
        self.graph.del_node(5)
        self.assertFalse(self.graph.has_edge(Edge(5, 11)))

        self.assertTrue(self.graph_undirected.has_edge(Edge(5, 11)))
        self.assertTrue(self.graph_undirected.has_edge(Edge(11, 5)))
        self.graph_undirected.del_node(5)
        self.assertFalse(self.graph_undirected.has_edge(Edge(5, 11)))
        self.assertFalse(self.graph_undirected.has_edge(Edge(11, 5)))

    def test_has_node(self):
        self.assertTrue(self.graph.has_node(1))
        self.assertTrue(self.graph.has_node(10))
        self.assertFalse(self.graph.has_node(2222))
        self.assertFalse(self.graph_undirected.has_node('ds'))
        self.assertTrue(self.graph_undirected.has_node(1))
        self.assertTrue(self.graph_undirected.has_node(10))
        self.assertFalse(self.graph_undirected.has_node(2222))

    def test_v(self):
        self.assertEqual(self.graph.v(), 17)
        self.assertEqual(self.graph_undirected.v(), 17)

    def test_e(self):
        self.assertEqual(self.graph.e(), 16)
        self.assertEqual(self.graph_undirected.e(), 16)

    def test_add_edge(self):
        self.graph.add_edge(Edge(1, 6, 5))
        with self.assertRaises(ValueError):
            self.graph.has_edge(Edge(111, 6, 5))
        self.assertFalse(self.graph.has_edge(Edge(6, 1, 6)))
        self.assertTrue(self.graph.has_edge(Edge(1, 6)))

        self.graph_undirected.add_edge(Edge(1, 6, 5))
        with self.assertRaises(ValueError):
            self.graph_undirected.has_edge(Edge(111, 6, 5))
        self.assertTrue(self.graph_undirected.has_edge(Edge(6, 1, 6)))
        self.assertTrue(self.graph_undirected.has_edge(Edge(1, 6)))

    def test_has_edge(self):
        self.assertTrue(self.graph.has_edge(Edge(2, 5)))
        self.assertFalse(self.graph.has_edge(Edge(10, 1)))
        with self.assertRaises(ValueError):
            self.graph.has_edge(Edge(114, 6))

    def test_del_edge(self):
        l_edges = self.graph.e()
        self.assertTrue(self.graph.has_edge(Edge(5, 12)))
        self.graph.del_edge(Edge(5, 12))
        self.assertFalse(self.graph.has_edge(Edge(5, 12)))
        self.assertEqual(l_edges - 1, self.graph.e())
        with self.assertRaises(ValueError):
            self.graph.del_edge(Edge(5, 12))

        self.assertTrue(self.graph_undirected.has_edge(Edge(5, 12)))
        self.assertTrue(self.graph_undirected.has_edge(Edge(12, 5)))
        self.graph_undirected.del_edge(Edge(5, 12))
        self.assertFalse(self.graph_undirected.has_edge(Edge(5, 12)))
        self.assertFalse(self.graph_undirected.has_edge(Edge(12, 5)))

    def test_weight(self):
        self.assertEqual(5, self.graph.weight(Edge(2, 6)))
        self.assertEqual(0, self.graph.weight(Edge(0, 6)))
        self.assertEqual(10, self.graph.weight(Edge(4, 10)))
        with self.assertRaises(ValueError):
            self.graph.weight(Edge(1111, 6))

    def test_copy(self):
        copy_graph = self.graph.copy()
        self.assertEqual(self.graph, copy_graph)

    def test_transpose(self):
        print self.graph.adjacent_matrix
        print self.graph.transpose().adjacent_matrix

    def test_iternodes(self):
        for node in self.graph.iternodes():
            print node,
        print '\n'

    def test_iteradjacent(self):
        for node in self.graph.iteradjacent(1):
            print node,
        print '\n'

    def test_iteroutedges(self):
        for node in self.graph.iteroutedges(1):
            print node,
        print '\n'

    def test_iterinedges(self):
        for node in self.graph.iterinedges(1):
            print node,
        print '\n'

    def test_iteredges(self):
        for node in self.graph.iteredges():
            print node,
        print '\n'

    def test_traverse_dfs(self):
        self.graph.traverse_dfs(0)

    def test_traverse_bfs(self):
        self.graph.traverse_bfs(0)

    def test_iter_dfs(self):
        for node in self.graph.iter_dfs(0):
            print node,
        print '\n'

    def test_iter_bfs(self):
        for node in self.graph.iter_bfs(0):
            print node,
        print '\n'


if __name__ == '__main':
    unittest.main()
