# coding=utf-8
class DFS:
    def __init__(self, graph, node):
        self.graph = graph
        self.node = node
        self.visited = []
        self.vertices_stack = [node]

    def __iter__(self):
        return self

    def next(self):
        if len(self.vertices_stack) > 0:
            self.node = self.vertices_stack.pop()
            if self.node not in self.visited:
                self.visited.append(self.node)
                for node in self.graph[self.node]:
                    if node not in self.visited:
                        if node not in self.vertices_stack:
                            self.vertices_stack.append(node)
            return self.node
        else:
            raise StopIteration
