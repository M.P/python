# coding=utf-8
from graph import Graph
from edge import Edge
from random import randint
from bfs import BFS
from dfs import DFS


graph = Graph(10, directed=True)
graph_undirected = Graph(10)

for i in range(5):
    graph.add_node(i)
    graph_undirected.add_node(i)


for i in range(graph.v()):
    for j in range(graph.v()):
        if i != j and randint(1, 100) % 3 == 0:
            graph.add_edge(Edge(i, j, randint(1, 10)))
            graph_undirected.add_edge(Edge(i, j, randint(1, 10)))

for i in graph.iternodes():
    print i

print graph.add_node(5)

print "Macierz sasiedztwa", graph.adjacent_matrix
print "Liczba wierzcholkow", graph.v()
print "Liczba krawedzi", graph.e()

print "Iterator po wierzcholkach sasiednich"
for i in graph.iteradjacent(2):
    print i

print "Iterator po krawedziach wychodzacych"
for i in graph.iteroutedges(2):
    print i

print "Iterator po krawedziach przychodzacych"
for i in graph.iterinedges(2):
    print i

print "Iterator po krawedziach"
for i in graph.iteredges():
    print i

graph1 = graph.transpose()
print "Liczba wierzcholkow", graph1.v()
print "Liczba krawedzi", graph1.e()
print "Transpose", graph1.adjacent_matrix
print "Oryginal", graph.adjacent_matrix
print "Weight", graph.weight(Edge(0, 1))
graph2 = graph.complement()
print "Complement", graph2.adjacent_matrix
print "Liczba krawedzi graph2", graph2.e()

graph3 = graph.subgraph([2, 1, 0])
print "Subgraph", graph3.adjacent_matrix
print "Liczba krawedzi graph3", graph3.e()

print "BFS"
bfs = BFS(graph, 2)
iter_bfs = iter(bfs)
for i in iter_bfs:
    print i

for i in graph.iter_bfs(2):
    print i

print "DFS"
dfs = DFS(graph, 2)
iter_dfs = iter(dfs)
for i in iter_dfs:
    print i

for i in graph.iter_dfs(2):
    print i

print "LEN ", len(graph)

print (2 in graph)

for node in graph:  # iterator po wierzchołkach
    print "wierzchołek", node

for node in graph_undirected:  # iterator po wierzchołkach
    print "wierzchołek", node
    print "stopien", len(graph_undirected[node])

print graph_undirected.adjacent_matrix

for target in graph_undirected[1]:  # iterator po sąsiadach
    print "data 1", "sąsiaduje z", target

print 1 in graph_undirected[2]  # bool, czy jest sąsiadem

for source in graph:  # iteracja po krawędziach bez wag
    for target in graph[source]:
        print "krawędź", (source, target)

print sum(len(graph[node]) for node in graph) #liczba krawędzi dla grafu skierowanego

print [node for node in graph if not graph[node]]
# lista wierzcholkow izolowanych

print sorted(len(graph_undirected[node]) for node in graph_undirected)
# posortowana lista stopni wierzcholkow grafu nieskierowanego

graph.traverse_dfs(2)
for i in graph.iter_dfs(2):
    print i

graph.traverse_bfs(2)
for i in graph.iter_bfs(2):
    print i

print graph.adjacent_matrix
print graph.complement().adjacent_matrix
