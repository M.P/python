import Queue


class BFS:
    def __init__(self, graph, node):
        self.graph = graph
        self.node = node
        self.visited = [False for i in range(graph.v())]
        self.vertices_queue = Queue.Queue()
        self.vertices_queue.put(self.node)
        self.visited[self.node] = True

    def __iter__(self):
        return self

    def next(self):
        if not self.vertices_queue.empty():
            self.node = self.vertices_queue.get()
            for node in self.graph[self.node]:
                if not self.visited[node]:
                    self.vertices_queue.put(node)
                    self.visited[node] = True
            return self.node
        else:
            raise StopIteration

